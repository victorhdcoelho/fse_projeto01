# FSE_projeto01

Para compilar o projeto use: `make`. Na raiz do projeto.

Para executar o projeto use: `./bin/bin`. Na raiz do projeto.

1. O primeiro número requerido e dos valores da histerese.
2. O segundo valor é no menu se vai colocar a temperatura que gostaria ou via terminal
ou via potenciômetro. Caso seja via terminal coloque a temporatura uma vez só (infelizmente
não coloquei a feature de continuar pedindo a temperatura o tempo todo via terminal).
3. Enquanto gera o arquivo csv ele mostrara no terminal os valores lidos.
4. Use `Ctrl + c` para parar a execução.
