#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#ifndef UART_COMUNICATION
#define UART_COMUNICATION
#define READ_TI 0xA1
#define READ_TR 0xA2


int open_uart()
{
	int uart = -1;
	uart = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
	if (uart == -1)
	{
		printf("Erro - Não foi possível abrir UART.\n");
	}
	else
	{
		printf("UART iniciada.\n");
	}
	struct termios opt;
	tcgetattr(uart, &opt);
	opt.c_cflag = B115200 | CS8 | CLOCAL | CREAD;     //<Set baud rate
	opt.c_iflag = IGNPAR;
    	opt.c_oflag = 0;
    	opt.c_lflag = 0;
    	tcflush(uart, TCIFLUSH);
    	tcsetattr(uart, TCSANOW, &opt);

	return uart;
}


float read_ti(int uart)
{
	char info[] = {READ_TI, 9, 4, 0, 1};
	float num = 0.0;

	if (uart != -1)
	{
		int count = write(uart, &info, sizeof(info));

		if(count < 0 )
		{
			printf("Erro na transmissão do float\n");
		}

	}

	if (uart != -1)
	{
		while(1){
		sleep(1);
		unsigned char floats[256];
		int rx_len = read(uart, (void *)floats, 4);
		if (rx_len < 0)
		{
			printf("Erro ao ler dados\n");
		}
		else if(rx_len == 0){
			printf("Sem dados disponiveis\n");
		}
		else
		{
			num = *(float*)floats ;
			printf("Bytes lidos: %d %f \n", rx_len, num);
			break;
		}
	}
	}
	return num;	
}

float read_tr(int uart)
{
	char info[] = {READ_TR, 9, 4, 0, 1};
	float num = 0.0;

	if (uart != -1)
	{
		int count = write(uart, &info, sizeof(info));

		if(count < 0 )
		{
			printf("Erro na transmissão do float\n");
		}

	}

	if (uart != -1)
	{
		while(1){
		sleep(1);
		unsigned char floats[256];
		int rx_len = read(uart, (void *)floats, 4);
		if (rx_len < 0)
		{
			printf("Erro ao ler dados\n");
		}
		else if(rx_len == 0){
			printf("Sem dados disponiveis\n");
		}
		else
		{
			num = *(float*)floats ;
			printf("Bytes lidos: %d %f \n", rx_len, num);
			break;
		}
	}}
	return num;	
}

#endif
