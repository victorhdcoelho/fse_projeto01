#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <string.h>

#ifndef CSV_UTILS
#define CSV_UTILS

void init_csv();
void insert_csv(float ti, float te, float tr);

void init_csv()
{
	FILE * csv = fopen("data.csv", "w");
	fprintf(csv, "data,ti,te,tr\n");
	fclose(csv);
}

void insert_csv(float ti, float te, float tr)
{
	FILE * csv = fopen("data.csv", "a");
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	char date[64];
	assert(strftime(date, sizeof(date), "%c", tm));
	fprintf(csv, "%s,%.2f,%.2f,%.2f\n", date, ti, te, tr);
	fclose(csv);
}

#endif
