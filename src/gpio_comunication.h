#include <bcm2835.h>
#include <stdio.h>

#ifndef GPIO_COMUNICATION
#define GPIO_COMUNICATION
#define COOLER RPI_GPIO_P1_18
#define RESIST RPI_GPIO_P1_16
void init_gpio();
void turn_on_cooler();
void turn_off_cooler();
void turn_on_resist();
void turn_off_resist();


void init_gpio(){
	if(bcm2835_init())
	{
		printf("Error ao iniciar gpio\n");
	}
	bcm2835_gpio_fsel(COOLER, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(RESIST, BCM2835_GPIO_FSEL_OUTP);
	// Começar sistema do 0
	bcm2835_gpio_write(COOLER, HIGH);
	bcm2835_gpio_write(RESIST, HIGH);
}

void turn_on_cooler()
{
	bcm2835_gpio_write(COOLER, LOW);	
}
void turn_off_cooler()
{
	bcm2835_gpio_write(COOLER, HIGH);
}

void turn_on_resist()
{
	bcm2835_gpio_write(RESIST, LOW);
}

void turn_off_resist()
{
	bcm2835_gpio_write(RESIST, HIGH);
}

#endif
