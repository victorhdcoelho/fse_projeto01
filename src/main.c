#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART
#include <signal.h>
#include "uart_comunication.h"
#include "i2c_comunication.h"
#include "gpio_comunication.h"
#include "csv_utils.h"

float ti = 0.0;
float tr = 0.0;
float te = 0.0;
float hist = 0.0;
pthread_t t1, t2, t3, t4, t5;
int choice;
int stop = 0;
int uart;	

void close_program(int sig);
void* read_potentiometer(void * arg);
void* read_temperatures(void* arg);
void* cooler(void* arg);
void* resist(void* arg);
void* lcd(void* arg);


int main(void)
{
	printf("Digite a margem da histerese: \n");
	scanf("%f", &hist);
	printf("1 - Potenciometro\n2 - Terminal\n");
	scanf("%d", &choice);
	hist /= 2;
	uart = open_uart();
	init_gpio();
	init_csv();
	if(choice == 1)
	{
		pthread_create(&t1, NULL, read_potentiometer, NULL);
	}
	else
	{
		printf("Temperatura desejada: \n");
		scanf("%f", &tr); 
	}
	signal(SIGINT, close_program);
	pthread_create(&t2, NULL, read_temperatures, NULL);
	pthread_create(&t3, NULL, cooler, NULL);
	pthread_create(&t4, NULL, lcd, NULL);
	pthread_create(&t5, NULL, resist, NULL);
	while(1)
	{
		if(stop == 1)
		{
			printf("Stop printing\n");
			break;
		}
		sleep(1);
		printf("TI: %.2f, TE: %2f, TR: %.2f\n", ti, te, tr);
	}
	if(choice == 1)
	{	
		pthread_join(t1, NULL);
	}
	pthread_join(t2, NULL);
	pthread_join(t3, NULL);
	pthread_join(t4, NULL);
	pthread_join(t5, NULL);
	close(uart);
	return 0;
}

void close_program(int sig)
{
	printf("Destruindo threads\n");
	stop = 1;
}

void* read_potentiometer(void* arg)
{
	while(1)
	{

		if(stop == 1)
		{
			printf("Stop read tr\n");
			break;
		}
		else{
		tr = read_tr(uart);
		}
	}
	return NULL;
}

void* read_temperatures(void* arg)
{
	while(1)
	{
		if(stop == 1)
		{
			printf("Stop read temperatures\n");
			break;
		}
		else
		{
		usleep(500);
		ti = read_ti(uart);
		te = read_te("/dev/i2c-1");
		}
	}
	return NULL;
}

void* cooler(void* arg)
{
	while(1)
	{
		if(stop == 1)
		{
			printf("Stop cooler\n");
			turn_off_cooler();
			break;
		}
		else
		{
			if (ti + hist > tr || ti - hist > tr)
			{
				turn_on_cooler();
			}
			else
			{
				turn_off_cooler();
			}
		}
	}
	return NULL;
}

void* resist(void* arg)
{
	while(1)
	{
		if (stop == 1)
		{
			printf("Stop resist\n");
			turn_off_resist();
			break;
		}
		else
		{
			if (ti - hist < tr)
			{
				turn_on_resist();
			}
			
			else
			{
				turn_off_resist();
			}
		}
	}
	return NULL;
}

void* lcd(void* arg)
{
	while(1)
	{
		
		if(stop == 1)
		{
			printf("Parando lcd\n");
			break;
		}
		else
		{
			print_on_lcd(ti, tr, te);
			sleep(1);
			insert_csv(ti, te, tr);
		}
	}
	return NULL;
}
